var swalert = function(msg, usroptions){
    var options = {
        title: 'Alert',
        text: null,
        html: msg,
        //timer: 2000,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancel',
        confirmButtonClass: "btn btn-success btn-fill",
        cancelButtonClass: "btn btn-danger btn-fill",
        buttonsStyling: false,
        on_ok: function(){console.log("OK")},
        on_cancel: function(dismiss){console.log("Canceled: "+dismiss)}
    };
    // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'

    Object.assign(options,usroptions);  // override options
    swal(options).then(options.on_ok, options.on_cancel);
};

var bsnotify = {
    success: function(message, usroptions) {
        var options = {type: "success",delay: 2000,placement: {from: "top", align: "left"}, z_index:1090};
        Object.assign(options,usroptions);  // override options
        return $.notify({icon: "ti-check-box", message: message}, options);
    },
    error: function(message, usroptions) {
        var options = {type: "danger",delay: 5000,placement: {from: "top", align: "left"}, z_index:1090};
        Object.assign(options,usroptions);  // override options
        return $.notify({icon: "ti-alert", message: message}, options);
    },
    warning: function(message, usroptions) {
        var options = {type: "warning",delay: 5000,placement: {from: "top", align: "left"}, z_index:1090};
        Object.assign(options,usroptions);  // override options
        return $.notify({icon: "ti-alert", message: message}, options);
    },
    info: function(message, usroptions) {
        var options = {type: "info",delay: 3000,placement: {from: "top", align: "left"}, z_index:1090};
        Object.assign(options,usroptions);  // override options
        return $.notify({icon: "ti-info-alt", message: message}, options);
    }
};

var bsmodal = function(url, usroptions) {
    var options = {
        modal_id: "modal-"+Date.now(),
        modal_class: "modal-lg",
        onshown: null,
        onhidden: null,
        oncompleted: null,
        iframe: false,
        show_close: true,
        remember_state: false
    };
    $.extend(options, usroptions);

    //if (typeof modal_id === 'undefined') modal_id = $("body.modal-open").find(".modal.in").attr('id');
    var $modal = $("#"+options.modal_id);
    var $modalcontent;
    if ($modal.length > 0) {
        $modalcontent = $modal.find(".modal-html");
    }
    else {
        var $close = (options.show_close) ? $("<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>") : $("");
        var body_padding = (options.show_close) ? "20px" : "0";
        $modalcontent = $("<div class='modal-html'>");
        $modal = $("<div id='" + options.modal_id + "' class='modal' tabindex='-1' role='dialog' data-backdrop='static'>").append(
            $("<div class='modal-dialog " + options.modal_class + "'>").append(
                $("<div class='modal-content'>").append(
                    $("<div class='modal-body' style='padding: "+body_padding+"'>").append(
                        $close,
                        $modalcontent
                    )
                )
            )
        );

        // Attach modal to body
        $('body').append($modal);

        // method for completion function passing data
        $modal.unbind('completed').bind('completed',function(e, data){
            e.stopPropagation();
            if (typeof options.oncompleted === 'function')
                options.oncompleted(data);
            $modal.modal("hide");
        });

        // method to reload this modal
        $modal.unbind('reload').bind('reload',function(e){
            //stop the event bubbling up to the parent
            e.stopPropagation();
            console.log("Reloading MODAL!", $modal);
            bsmodal(url, options);
        });

        // Remove it from DOM on close (if set to not remember)
        $modal.on('hidden.bs.modal', function(){
            if (typeof options.onhidden === 'function') {
                options.onhidden.call(this);
            }
            if (!options.remember_state) {
                $modal.remove();
                console.log("removed modal id="+options.modal_id);
            }
        });
    }

    $modal.modal("show", {backdrop: options.backdrop});
    if (!options.remember_state || $modal.data("url") !== url) {
        if (options.iframe) {
            var $iframe = $("<iframe id='modal-iframe' src='" + url + "' style='height:100%;width:100%;border:none; background-color: transparent; overflow: auto;'></iframe>");
            $modalcontent.html($iframe);
            $iframe.on('load', function () {
                $iframe.height($modal.innerHeight());
                if (typeof options.onshown === 'function') options.onshown.call($modal);
            });
        }
        else {
            //$modalcontent.html(""); //.hide();
            $modalcontent.loadcontent(url, {
                success: function (data) {
                    if (typeof options.onshown === 'function') options.onshown.call($modal)
                },
                bind_reload: false
            });
        }
    }

    $modal.data({options: options, url:url});
    return $modal;
};


var path = {
    basename: function(p) {
        return p.replace(/\\/g,'/').replace( /.*\//, '' );
    },
    dirname: function(p) {
        return p.replace(/\\/g,'/').replace(/\/[^\/]*$/, '');
    }
};


$.fn.loadcontent = function(url, usroptions) {
    var $container = this;
    var options = {
        success: null,
        show_loading: null,
        reload_interval: null,
        reload_on_error: true
    };
    $.extend(options, usroptions);

    if (options.show_loading || !$container.data("_has_loaded") && options.show_loading===null) {
        $container.html($('<div>')
            .css({'text-align': 'center', 'padding': '50px 0 50px 0'})
            .append('<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i><br>loading...')
        ).data("_has_loaded", true);
    }

    // attach reload function
    $container.unbind('reload').bind('reload', function (e) {
        e.stopPropagation();
        if ($container.is(":visible")) {
            if ($container.length > 0 && $container.is(":visible"))
                $container.loadcontent(url, options);
        }
    });

    $.ajax({
        url: url,
        cache: false,  // prevent caching
        dataType: "html",
        success: function(data) {
            $container.html(data);
            if (typeof options.success === 'function') options.success(data);

            $container.find("a[rel='gui']").click(link_click_handler.gui);
            $container.find("a[rel='gui_warn']").click(link_click_handler.gui_warn);
            $container.find("a[rel='modal']").click(link_click_handler.modal);
            $container.find("a[rel='bgprocess']").click(link_click_handler.bgprocess);

            // reload option is set, reload on interval
            if (options.reload_interval && $container.is(":visible")) {
                setTimeout(function () {
                    $container.trigger("reload");
                }, options.reload_interval);
            }
            // if loaded successfully, clear error timeout to prevent reload attempt
            if ($container.data().hasOwnProperty("error_reload_timer"))
                clearTimeout($container.data("error_reload_timer"));
        }
    }).fail(function(xhr, textStatus, errorThrown){
        console.log(xhr, textStatus, errorThrown);
        var msg = (xhr.hasOwnProperty("responseJSON")) ? (xhr.responseJSON.hasOwnProperty("message")) ? xhr.responseJSON.message : xhr.responseText : xhr.responseText;
        $container.html($('<div class="well well-sm text-danger alert-with-icon" data-notify="container">').append(
            $('<span data-notify="icon" class="ti-alert"></span>'),
            $('<span data-notify="message" title="'+url+'">').html(
                "Unable to load content (STATUS: "+xhr.status + ", " + xhr.statusText+", "+msg+")"
            )));

        // If failed to load, try again in 10s
        if (options.reload_on_error) {
            console.log("Reloading on error!", $container);
            $container.data("error_reload_timer", setTimeout(function () {
                $container.trigger("reload");
            }, 10000));
        }
    });

    return this;
};


var extgui = {
    open: function(url){
        url += ( url.match( /[\?]/g ) ? '&' : '?' ) + '_='+Date.now();   // bust cache by adding timestamp
        $.get(url).fail(function(xhr, textStatus, errorThrown){
            bsnotify.error("Unable to open external GUI component for <a href='"+url+"' target='_blank' title='"+url+"'>URL</a> (STATUS: "+xhr.status + ", " + xhr.statusText+")");
        });
        return false;
    },
    open_warning:function(url){
        swalert(
            "<p class='text-left'>You are about to open a file in your default external editor for this file type. If " +
            "you do not have a default editor configured on your operating system for this file type, you may need to " +
            "do that first.</p><p class='text-left text-danger'><b><i class='fa fa-exclamation-triangle'></i> This offers " +
            "more flexibility but can be risky. Do not change file names (eg: use 'save' not 'save as') and do not " +
            "modify file formats when editing files externally. If you're not confident with this, click cancel.</b></p>",
            {
                title: "External Editor Warning!",
                on_ok: function () {
                    extgui.open(url);
                }
            }
        );
    },
    bgprocess: function(url, success, fail) {
        var _this = this;
        url += ( url.match( /[\?]/g ) ? '&' : '?' ) + '_='+Date.now();   // bust cache by adding timestamp
        $.getJSON(url, function(data){
            if (typeof success === "function") success.call(_this, data);
        }).fail(function(xhr, textStatus, errorThrown){
            console.log(xhr);
            var msg = (xhr.hasOwnProperty("responseJSON")) ? (xhr.responseJSON.hasOwnProperty("message")) ? xhr.responseJSON.message : xhr.responseText : xhr.statusText;
            bsnotify.error("There was an error completing this request: "+msg);
            if (typeof fail === "function") fail.call(_this, xhr, textStatus, errorThrown);
        });
    }
};

var link_click_handler = {
    modal: function(e) {
        e.preventDefault();
        bsmodal.call(this, $(this).attr("href"), $(this).data());
        return false;
    },
    gui: function(e) {
        e.preventDefault();
        extgui.open.call(this, $(this).attr("href"));
        return false;
    },
    gui_warn: function(e) {
        e.preventDefault();
        extgui.open_warning.call(this, $(this).attr("href"));
        return false;
    },
    bgprocess: function(e) {
        e.preventDefault();
        var endpoint = ($(this).data().hasOwnProperty("endpoint")) ? $(this).data("endpoint") : $(this).attr("href");
        extgui.bgprocess.call(this, endpoint, $(this).data("success"));
        return false;
    }
};

