from PyInstaller.building.build_main import Analysis

block_cipher = None

a = Analysis(
    [
        'ui.py',
        'plugins/filesystem.py',
        'plugins/metadata.py',
        'plugins/dataset.py',
        'plugins/globalarchive.py',
        'plugins/ftp.py',
        'config/settings.py'
    ],
    pathex=['/Users/ariell/projects/squidlelib_synctool'],
    binaries=[],
    datas=[
        ('config','config'),
        ('plugins','plugins'),
        ('static','static'),
        ('templates','templates'),
        ('version', 'version')
    ],
    hiddenimports=[],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False
)

