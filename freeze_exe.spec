# -*- mode: python -*-

# add your project directory to the sys.path
#sys.path.insert(0, ".")

# import all the stuff
from freeze_spec import a, block_cipher

pyz = PYZ(
    a.pure,
    a.zipped_data,
    cipher=block_cipher
)

exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.zipfiles,
    a.datas,
    [], #[('v', None, 'OPTION')],
    name='Synctool',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    runtime_tmpdir=None,
    console=False,
    icon='icons/icon.ico'
)