"""Synctool UI module

Additional endpoints and methods for handling UI components of Synctool application
"""
import platform
from threading import Thread  #, Lock
#import logging
import subprocess
import webview
from time import sleep
from flask import jsonify
import os
from server import start_server, app, run_plugin_method, request, is_alive

#######################################################################################################################
# Some globals
#######################################################################################################################
START_SERVER_DELAY = 10                 # Max s to wait for server to start
CHECK_SERVER_POLL_DELAY = 0.5           # Period of polling in s to check server status
url = "http://localhost:{}/".format(app.config.get("PORT"))
fatal_error = None
waiting = True

# Logging
#logger = logging.getLogger(__name__)
#logger.setLevel(logging.DEBUG)


#######################################################################################################################
# Application UI endpoints
#######################################################################################################################
@app.route("/modal/file", defaults={"plugin": None, "method": None})
@app.route("/modal/file/<plugin>/<method>")
def choose_files(plugin, method, *args, **kwargs):
    """

    :param plugin:
    :param method:
    :return:
    """
    file_type = request.args.get("file_type", None)
    if file_type is None:
        files = webview.create_file_dialog(webview.FOLDER_DIALOG)
    else:
        files = webview.create_file_dialog(webview.OPEN_DIALOG, allow_multiple=False, file_types=(file_type,))
    response = {"status": "cancel"}
    selected_file = None
    if files and len(files) > 0:
        selected_file = files[0]
        if isinstance(selected_file, bytes):
            selected_file = selected_file.decode("utf-8")
        if plugin is not None and method is not None:
            response = run_plugin_method(plugin, method, kwargs={"directory": selected_file})
        else:
            response = {"status": "done"}
    return jsonify(selected_file=selected_file, **response)


@app.route("/open/file")
def open_file(*args, **kwargs):
    path = request.args.get("path", "")
    try:
        if platform.system() == "Windows":
            subprocess.check_call(['explorer', path])
        elif platform.system() == "Darwin":
            subprocess.Popen(["open", path])
        else:
            subprocess.Popen(["xdg-open", path])
        return jsonify(status="done", path=path)
    except Exception as e:
        return jsonify(status="error", path=path, message="ERROR: {}".format(e))


#######################################################################################################################
# Handy methods
#######################################################################################################################
def check_status(t):
    # check server is alive and display error if process dies
    for i in range(int(START_SERVER_DELAY/CHECK_SERVER_POLL_DELAY)):
        if is_alive() and t.isAlive():
            print("Server is alive!")
            webview.load_url(url)
            break
        else:
            print("Waiting for server to start...")
        sleep(CHECK_SERVER_POLL_DELAY)
    else:
        load_error_html("Server could not be started... Timed out.")

    while True:
        sleep(5)
        if not webview.window_exists():
            print("Window is closed, killing all threads")
            os._exit(1)
        else:
            if not t.isAlive():
                load_error_html("Application server thread does not appear to be running...")

def load_error_html(error):
    print("Loading error HTML...")
    template = app.jinja_env.get_template("/synctool/initialising.html")
    webview.load_html(template.render({"error_msg": error, "port": app.config.get("PORT")}), uid="master")
    #webview.load_html(html % (error, app.config.get("PORT")))

def load_loader_html():
    print("Loading...")
    template = app.jinja_env.get_template("/synctool/initialising.html")
    webview.load_html(template.render(), uid="master")

if __name__ == '__main__':
    print("Starting server...")
    t = Thread(target=start_server, daemon=True)
    t.start()

    print("Starting health-check process...")
    t2 = Thread(target=check_status, args=[t], daemon=True) 
    t2.start()
    
    #launch_gui()
    webview.create_window("SyncTOOL", background_color='#333333', width=1300, height=800, min_size=(800, 600))
    

    
