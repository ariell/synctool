# SYNCTOOL #

Freezable python-based application for managing, validating and
processing marine image and marine image metadata in preparation for
uploading to cloud storage data repositories and online platforms.

This is the source code, but the software tool will be available
as a stand-alone, downloadable application for windows, mac and linux.

### Quickstart ###
##### Installation:
```bash
cd synctool                 # change to project dir
python3 -m venv env
source env/bin/activate     # activate environment
pip install -r requirements.txt   # install dependencies
```

Note: this project uses [pywebview](https://github.com/r0x0r/pywebview)
for the UI components. Depending on your OS, you may need additional
libraries. Refer to [here](https://pywebview.flowrl.com/guide/installation.html) for more info.


##### Building on OSX:
Initial setup
```
# do this before the next step!
pip install -r requirements-freeze_app.txt

# copy system binary to venv to fix osx issue with window focus
# NOTE: FIRST TRY WITHOUT THIS
#cp -L `which python3` env/bin
```

Building:
```
bash freeze_app.sh
```

##### Building on WINDOWS:
Initial setup
```
pip install -r requirements-freeze_exe.txt
```

Building:
```
bat freeze_exe.bat
```

##### Syncing installer files to cloud
bash sync_dist_versions.sh

##### Running server module:
```bash
python server.py
```
That spins up an instance of the local server, which can be accessed
through a web browser on your local machine.

##### Running UI (also starts server module)
```bash
python ui.py
```

##### Freezing app for deployment
Once you have a working local installation, you can build the standalone app
```bash
bash freeze_app.sh
```
NOTE: this needs to be done on the target operating system (it is not possible to cross-compile)

### Code structure ###
There are two main entry points which control the app: server.py and
ui.py. Most of the meaty goodness is contained in the plugins modules
which can be used to flexibly define endpoints for the API and UI.
JSON responses from API endpoints can be accessed in raw form, eg:
```
/api/<plugin>/<method>?param1=val1&param2=val2
```
or they can be formatted using templates, eg:
```
/api/<plugin>/<method>?param1=val1&param2=val2&template=path/to/template.html
```
Templates can also be accessed standalone:
```
/ui/<path:template>
```

More details on all of this to come.

##### Adding a new online platform / plugin #####
The code is written to allow custom modules and
plugins to be added relatively easily. This involves writing a custom
code module and then referencing it in the config file. The new
endpoints will be available through the API automatically. If there are
UI components, you will need to specify a template directory and define
templates. More details on this still to come.


### More info ###
Note: this documentation is not complete. More info still to come
