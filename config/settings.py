import os
import sys
from appdirs import user_config_dir, user_data_dir
from shutil import copyfile


config_file = os.path.realpath(__file__)
config_path = os.path.dirname(config_file)
root_path = os.path.dirname(config_path)

default_data_dirname = "mydata"
user_settings_filename = "settings.json"  #"settings.cfg"

class Config(object):
    # All of these can be overriden in the user config file
    PLUGINS = {
        'filesystem': {
            'ref': "plugins.filesystem.FileSystem",
            'args': [],
            'kwargs': {}
        },
        # 'project': {'ref': "plugins.projects.Project", 'args': [], 'kwargs': {}},
        'datafile': {
            'ref': "plugins.metadata.DataFile",
            'args': [],
            'kwargs': {}
        },
        #'sftp': {'ref': "plugins.ftp.SFTPClient", "args": [], "kwargs": dict(host_server=sftp_host, port=22, username=sftp_user, password=sftp_pass, basedir="staging/")}  # TODO: this credentials should be retrieved from marinedb api using API key
    }
    PLATFORM_PLUGINS = {}
    HOST = "0.0.0.0"
    PORT = 5001
    APPNAME = "SyncTOOL"
    PLATFORM_PLUGINS = {
        "globalarchive_aodn": {
            "name": "GlobalArchive AODN",
            "template": "globalarchive",
            "plugin": "plugins.globalarchive.GA",
            "api_key": "",
            "host_url": "https://globalarchive.org"
        },
        "squidle_aodn": {
            "name": "SQUIDLE+ AODN",
            "template": "squidle",
            "plugin": "plugins.globalarchive.GA",
            "api_key": "",
            "host_url": "http://squidle.org"
        }
    }
    PATHSEP=os.sep   # path separator for OS

class FrozenAppConfig(Config):
    HOME_PATH = os.path.expanduser("~")
    # Detect if frozen into app, and setup things up for frozen environment
    FROZEN_CONFIG_DIR = user_config_dir(Config.APPNAME)
    FROZEN_USERDATA_DIR = user_data_dir(Config.APPNAME)

    EXECUTABLE_PATH = getattr(sys, '_MEIPASS', root_path)
    APPLICATION_PATH = os.path.dirname(sys.executable)
    USER_CONFIG = os.path.join(FROZEN_CONFIG_DIR, user_settings_filename)
    PROJECT_PATH = os.path.join(FROZEN_USERDATA_DIR, default_data_dirname)

    # make dirs and copy config if doesn't exist
    if not os.path.isdir(FROZEN_CONFIG_DIR):
        os.makedirs(FROZEN_CONFIG_DIR)
    if not os.path.isdir(PROJECT_PATH):
        os.makedirs(PROJECT_PATH)
    if not os.path.isfile(USER_CONFIG):
        copyfile(os.path.join(config_path, user_settings_filename), USER_CONFIG)

    print(f"EXECUTABLE_PATH: {EXECUTABLE_PATH}")

class DevConfig(Config):
    EXECUTABLE_PATH = root_path
    APPLICATION_PATH = root_path
    USER_CONFIG = os.path.join(config_path, user_settings_filename)
    PROJECT_PATH = os.path.join(root_path, default_data_dirname)
