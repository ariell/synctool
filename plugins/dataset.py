import csv
import fnmatch
import glob
import os

# import pandas as pd
import requests
from uuid import uuid4

import time
from werkzeug.utils import secure_filename

from server import APIException
from plugins.filesystem import FileSystem, FILEOP_MKDIR, FILEOP_CP, FILEOP_LN, FILEOP_RM, is_hidden_file  #, LINKDIR_FILE
# from .ftp import TFStatus

record_ref_file_pattern = os.path.join("{project_path}",".record.json")
localinfo_file_pattern = os.path.join("{project_path}",".info.csv")
localprojname_file_pattern = os.path.join("{project_path}",".projectname.txt")
local_metadata_pattern = os.path.join("{project_path}","*_Metadata.csv")


local_media_dir_pattern = os.path.join("{project_path}","media","")
local_metadata_dir_pattern = os.path.join("{project_path}","metadata")
local_campaign_data_pattern = os.path.join("{project_path}","campaign_data.json")
MEDIAINFO_FILE_PATTERN = os.path.join("{project_path}", "media_metadata.csv")



class Platform(FileSystem):

    def __init__(self, settings={}, api_key=None, host_url="https://globalarchive.org", instance_id=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.settings = settings
        self.api_key = api_key
        self.host_url = host_url
        self.instance_id = instance_id
        self.user = None


    def create_edit_campaign(self, data, campaign_path=None, *args, **kwargs):
        """

        :param data:
        :param campaign_path:
        :param args:
        :param kwargs:
        :return:
        """
        # get directories
        proj_path = self.settings.get("PROJECT_PATH")
        if not campaign_path:
            campaign_path = os.path.join(proj_path, str(uuid4()))
        metadata_dir = local_metadata_dir_pattern.format(project_path=campaign_path)
        media_dir = local_media_dir_pattern.format(project_path=campaign_path)

        campaign_uuid = os.path.basename(campaign_path)

        # create directories (if they do not already exist)
        self.fileop(path=campaign_path, op=FILEOP_MKDIR)
        self.fileop(path=metadata_dir, op=FILEOP_MKDIR)
        self.fileop(path=media_dir, op=FILEOP_MKDIR)


        # Create campaign name from parameters
        # TODO: NAME MASHING SHOULD GO TO THE CUSTOM PLUGIN, NOT THE GENERIC PLATFORM CLASS
        if "name" not in data:
            site_name = data.get("site_name", "Site.Name").replace(" ", ".")
            platform_name = data.get("platform",{}).get("name","PLATFORM")
            data["name"] = secure_filename(f"{data.get('start_date','YYYY-MM-DD')}_{site_name}_{platform_name}")

        # copy / link files
        for f in data.get("metadata"):
            dest = os.path.join(metadata_dir, f.get("pattern").replace("*", ""))
            fpath = self.import_files(f.get("origin", None), dest, op="copy")
            f["path"] = os.path.join(campaign_uuid,fpath.split(campaign_uuid)[1][1:])  # change to relative path
            # origin = f.get("origin", None)
            # if origin:  # copy/link files
            #     self.fileop(origin, dest=f["path"], op=FILEOP_CP if f.get("op") == "copy" else FILEOP_LN)
            # else:       # remove if previously linked and now unlinked (null origin posted)
            #     if os.path.isfile(f["path"]):
            #         self.fileop(f["path"], op=FILEOP_RM)

        # copy / link files in directory
        # media = data.pop("media")
        for d in data.get("media"):
            d["path"] = self.add_media_files(d.get("origin", None), campaign_path, data=data)

        campaign_data_file = local_campaign_data_pattern.format(project_path=campaign_path)
        self.write_json(campaign_data_file, data=data)
        return dict(data_file=campaign_data_file, data=data, path=campaign_path)

    def import_files(self, origin, dest, op="copy", prefix="", *args, **kwargs):
        path = dest
        if origin:
            if os.path.isdir(origin):
                for f in glob.glob(os.path.join(origin, "*")):  # expand directory into list of files
                    if not is_hidden_file(f):            # ignore hidden / strange folders
                        if os.path.isdir(f):
                            self.import_files(f, dest, op=op, prefix=os.path.basename(f)+"_")
                        else:
                            d = os.path.join(path, prefix+os.path.basename(f)) if os.path.isdir(path) else path
                            self.fileop(path=f, dest=d, op=FILEOP_LN if op == "link" else FILEOP_CP)
                #open(os.path.join(dest, LINKDIR_FILE), 'w').close()   # create empty file to show that files are linked
            elif os.path.isfile(origin):
                if os.path.isdir(dest):
                    path = os.path.join(dest, prefix+os.path.basename(origin))
                self.fileop(path=origin, dest=path, op=FILEOP_LN if op == "link" else FILEOP_CP)
        else:  # remove if previously linked and now unlinked (null origin posted)
            if os.path.isfile(path):
                self.fileop(path=path, op=FILEOP_RM)
        return path

    def add_media_files(self, origin, campaign_path, data=None, *args, **kwargs):
        media_import_field = "media_imports"
        media_dir = local_media_dir_pattern.format(project_path=campaign_path)
        campaign_data_file = local_campaign_data_pattern.format(project_path=campaign_path)
        # self.write_json(campaign_data_file, data=data)
        self.import_files(origin, media_dir, op="link")
        if data is None:
            data = self.read_json(campaign_data_file)
        if media_import_field in data:
            data[media_import_field].append(origin)
        else:
            data[media_import_field] = [origin]
        self.write_json(campaign_data_file, data=data)
        return media_dir

    def get_campaign(self, campaign_path, *args, **kwargs):
        """

        :param campaign_path:
        :param args:
        :param kwargs:
        :return:
        """
        data_file = local_campaign_data_pattern.format(project_path=campaign_path)              # get data file path
        metadata_dir = local_metadata_dir_pattern.format(project_path=campaign_path)            # get metadata dir
        data = self.read_json(data_file)                                                        # get campaign data
        data["path"] = campaign_path                                                            # set path
        data["uuid"] = os.path.basename(campaign_path)                                          # set UUID for campaign
        # enforce relative path reference (in case copied from one machine to another)
        for f in data.get("metadata", []):
            fpath = os.path.normpath(f["path"])
            f["path"] = os.path.join(self.settings.get("PROJECT_PATH"), data["uuid"], fpath.split(data["uuid"])[1][1:])
        self.run_time(self.preprocess_campaign, data)                                                          # run pre val ops
        self.run_time(self.match_files, data.get("metadata", []), metadata_dir, prefix=data.get("name",""))    # match metadata files
        self.run_time(self.match_files, data.get("media", []), campaign_path, prefix=data.get("name",""))      # match media files
        self.run_time(self.validate_campaign, data)                                                            # run validation
        return data

    def run_time(self, fnc, *args, **kwargs):
        tic = time.time()
        print(f"Running: '{fnc.__name__}'...", end="")
        retval = fnc(*args, **kwargs)
        print(f"Done in {time.time()-tic} s")
        return retval

    def preprocess_campaign(self, data, *args, **kwargs):
        pass

    def get_validation_params(self, refresh=False, pattern=None):
        raise NotImplementedError("This method has not been implemented. It should be overridden in parent class.")

    def validate_campaign(self, data, *args, **kwargs):
        data["errors"] = []
        data["warnings"] = []

        # Validate data files
        file_list = data.get("metadata", [])
        for f in file_list:
            fpattern = f.get("pattern")
            fdata = self._get_datafile_from_pattern(file_list, fpattern)
            fpath = self._get_datafile_path(fdata)
            file_validation_params = self.get_validation_params(refresh=False)
            val_params = file_validation_params.get(fpattern, {})
            valdata = self.validate_file(fpath, val_params, file_list=file_list)

            f["errors"] = valdata.get("errors", [])
            f["warnings"] = valdata.get("warnings", [])
            data["errors"] += ["Error in '{}' file: {}".format(f.get("pattern"), e) for e in f["errors"]]
            data["warnings"] += ["Warning in '{}' file: {}.".format(f.get("pattern"), w) for w in f["warnings"]]

        # Validate media and media info
        data["mediainfo_file_path"] = MEDIAINFO_FILE_PATTERN.format(project_path=data.get("path"))
        media_dirs = data.get("media", [])
        media_metadata_file = local_media_dir_pattern.format(project_path=data.get("path"))
        for d in media_dirs:
            valdata = self.match_media_metadata_to_dir(data["mediainfo_file_path"], media_metadata_file, extended=False)
            # d["mismatch_file"] = os.path.join(data.get("path"), "_mismatched_media_list.csv")
            # mismatch_df = pd.concat([pd.DataFrame({"not_in_media_dir": valdata.get("mismatch_in_file")}),
            #                          pd.DataFrame({"not_in_data_file": valdata.get("mismatch_in_dir")})], axis=1)
            # mismatch_df.to_csv(d["mismatch_file"], index=False)
            d.update(valdata)
            data["errors"] += ["Error matching files in '{}': {}".format(d.get("pattern"), e) for e in d["errors"]]
            data["warnings"] += ["Warning matching files in '{}': {}.".format(d.get("pattern"), w) for w in d["warnings"]]


    def get_user(self, *args, **kwargs):
        raise NotImplementedError("This method has not been implemented. It should be overridden in parent class.")

    def match_files(self, filedata, directory, prefix="", *args, **kwargs):
        file_list = os.listdir(directory) if os.path.isdir(directory) else None
        for f in filedata:
            matched_list = fnmatch.filter(file_list, f.get("pattern")) if file_list else []
            f["matched"] = [os.path.join(directory, i) for i in matched_list]
            f["name"] = "{}_{}".format(secure_filename(prefix), matched_list[0]) if matched_list else None

    # api_record_url = "http://globalarchive.org/api/campaign-full/{id}"
    def inspect_project_file(self, finfo, *args, **kwargs):
        # localprojname_file = localprojname_file_pattern.format(project_path=finfo["path"])
        if finfo["type"] == "dir":
            data_file = local_campaign_data_pattern.format(project_path=finfo["path"])
            metadata_dir = local_metadata_dir_pattern.format(project_path=finfo["path"])
            media_dir = local_media_dir_pattern.format(project_path=finfo["path"])
            data = self.read_json(data_file) if os.path.isfile(data_file) else {}
            finfo["nfiles"] = len(next(os.walk(metadata_dir))[2]) if os.path.isdir(metadata_dir) else 0
            finfo["nmedia"] = len(next(os.walk(media_dir))[2]) if os.path.isdir(media_dir) else 0
            finfo["name"] = data.get("name")
            finfo["project_name"] = data.get("project", {}).get("name")
            finfo["platform_name"] = data.get("platform", {}).get("name")

            #finfo["project_name"] = open(localprojname_file,'r').read().strip() if os.path.isfile(localprojname_file) else ""
            # Add integrity checks to output
            # finfo.update(self.check_data_integrity(finfo["path"]))
            finfo["data_checks"] = self.check_data_integrity(finfo["path"])

    def list(self, workingdir, *args, **kwargs):
        return self.browse(workingdir +"/*", filters=[dict(field="type",op="eq",val="dir")], inspect_function=self.inspect_project_file)

    def detail(self, path, *args, **kwargs):
        localinfo_file = localinfo_file_pattern.format(project_path=path)
        localprojname_file = localprojname_file_pattern.format(project_path=path)
        api_record_file = record_ref_file_pattern.format(project_path=path)
        metadata_file = glob.glob(local_metadata_pattern.format(project_path=path))[0] \
            if len(glob.glob(local_metadata_pattern.format(project_path=path))) > 0 else None

        localinfo = {i["name"]:i["value"] for i in csv.DictReader(open(localinfo_file))} if os.path.isfile(localinfo_file) else {}
        projectname = open(localprojname_file,'r').read().strip() if os.path.isfile(localprojname_file) else None
        api_record_id = self.read_json(api_record_file, "id")[0] if os.path.isfile(api_record_file) else None

        return dict(
            api_record_id=api_record_id,
            local_metadata={},
            local_files=self.browse(path+"/*"),
            name=os.path.basename(path),
            integrity_checks=self.check_data_integrity(path),
            local_info=localinfo,
            project_name=projectname,
            metadata_file = metadata_file
        )

    def check_data_integrity(self, path, *args, **kwargs):
        try:
            tfstat = self.sftp_client.status(source=path)
            upload_status = "complete" if tfstat.get("is_complete") \
                else "in-progress" if tfstat.get("is_active") \
                else "none" if not tfstat.get("started_at") \
                else "error" if tfstat.get("error") \
                else "unknown"
            syncstat = self.get_online_record_syncstat(path)
            sync_status = "complete" if syncstat.get("synced_at") else "none"
            return dict(upload_status=upload_status, sync_status=sync_status)
        except:
            return None

    # def get_api_record(self, id):
    #     r = requests.get(self.api_record_url.format(id=id), headers={"Accept":"application/json"})
    #     if not r.ok:
    #         raise ChildProcessError("Response code: {}, {}".format(r.status_code, r.text))
    #     return r.json()  #, r.status_code

    def get_json_api(self, endpoint, qs=None, append_host=True, *args, **kwargs):
        url = endpoint + "?" + qs if qs is not None else endpoint
        if append_host:
            url = self.host_url + url
        print(f"Getting URL: {url}")
        r = requests.get(url, headers={"auth-token": self.api_key})
        if not r.ok:
            raise APIException("Request failed: {} (reason: {})".format(endpoint, r.reason), code=r.status_code)
        return dict(url=url, data=r.json())

    def assert_logged_in(self, *args, **kwargs):
        if self.user is None:
            self.get_user()

        assert self.user is not None, "You are not logged in. Check your API KEY."


    # def _get_datafile_from_pattern (self, file_list, pattern):
    #     for f in file_list:
    #         if pattern == f.get("pattern"):
    #             return f
    #     return {}

    def _get_datafile_path(self, filedata):
        # TODO: get from matched files instead of path
        matched_list = filedata.get("matched", [])
        first_match = matched_list[0] if matched_list else None
        return first_match
        # return filedata.get("path", None)
