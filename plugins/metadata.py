import csv
import os

# import numpy as np
# from pandas import DataFrame
import pandas as pd
# from datetime import datetime
import urllib.parse

class DataFile:

    def __init__(self):
        pass

    def infer_delimiter(self, fname):
        return "," if os.path.splitext(fname)[1] == ".csv" else "\t"

    def open(self, f, delimiter=None, format="list", *args, **kwargs):
        f = urllib.parse.unquote_plus(f)

        if delimiter is None:  # infer based on extension
            delimiter = self.infer_delimiter(f)

        data = []
        columns = []
        if os.path.isfile(f):
            print(f"FILE: {f}")
            df = pd.read_csv(f, delimiter=delimiter, index_col=False)
            df = df.replace({pd.np.nan: None})
            columns = list(df.columns)
            if format == "list":
                data = df.values.tolist()
            elif format == "dict":
                data = df.to_dict(orient="records")
        else:
            print(f"NO FILE FOUND: {f}")
        return dict(delimiter=delimiter, data=data, columns=columns)

    def save(self, f, data=None, headers=None, delimiter=None):
        if delimiter is None:  # infer based on extension
            delimiter = self.infer_delimiter(f)
        with open(f, 'w') as myfile:
            wr = csv.writer(myfile, quoting=csv.QUOTE_NONNUMERIC, delimiter=delimiter)
            wr.writerows([headers]+data)

        return dict(message="Successfully saved file: {}".format(os.path.basename(f)))




    # def file2df(self, path, sep=None):
    #     if sep is None:
    #         sep = "," if path.name.strip().endswith(".csv") else "\t"
    #     return DataFrame.read_csv(path, sep=sep, parse_dates=False, index_col=False)
    #
    # def validate(self, path, columns, sep=None):
    #     valdf = self.file2df(path)
    #     # print (valdf)
    #     nrows = valdf.shape[0]
    #     for k in columns:
    #         if k in valdf.columns and nrows > 0:
    #             if 'type' in columns[k]:
    #                 if columns[k]['type'] == 'float' and valdf[k].dtype != np.float64:
    #                     raise ValueError(
    #                         "Invalid data found in '{}'. The '{}' column needs to be of type: 'float'.".format(
    #                             path.name, k))
    #                 if columns[k]['type'] == 'int' and valdf[k].dtype != np.int64:
    #                     raise ValueError(
    #                         "Invalid data found in '{}'. The '{}' column needs to be of type: 'integer'.".format(
    #                             path.name, k))
    #                 if columns[k]['type'] == 'number' and valdf[k].dtype != np.number and valdf[k].dtype != np.int64 and valdf[k].dtype != np.float64:
    #                     raise ValueError(
    #                         "Invalid data found in '{}'. The '{}' column needs to be of type: 'number'.".format(
    #                             path.name, k))
    #             if 'format' in columns[k] and columns[k]['type'] == 'datetime':
    #                 try:
    #                     valdf[k].apply(lambda x: datetime.strptime(str(x), columns[k]['format']))
    #                 except Exception as e:
    #                     raise ValueError(
    #                         "Invalid data found in '{}'. The '{}' column needs to be: 'datetime' with format '{}'.".format(
    #                             path.name, k, columns[k]['format']))
    #         if 'isini' in columns[k] and not valdf[k].str.lower().isin(
    #                 [str(x).lower() for x in columns[k]['isini']]).all():
    #             raise ValueError(
    #                 "Invalid data found in '{}'. The '{}' column must be one of: {}.".format(path.name, k,
    #                                                                                          ", ".join(columns[k][
    #                                                                                                        'isini'])))
    #         if 'isin' in columns[k] and not valdf[k].isin(columns[k]['isin']).all():
    #             raise ValueError(
    #                 "Invalid data found in '{}'. The '{}' column must be one of: {} (case sensitive).".format(
    #                     path.name, k, ", ".join(columns[k]['isin'])))
    #         if columns[k].get('isunique', False) and valdf[k].duplicated().any():
    #             raise ValueError(
    #                 "Duplicate data found in '{}'. The '{}' column must be unique.".format(path.name, k))
    #         if 'range' in columns[k] and not valdf[k].between(*columns[k]['range']).all():
    #             raise ValueError(
    #                 "Invalid data found in '{}'. The '{}' column must have values between '{}' and '{}'.".format(
    #                     path.name, k, *columns[k]['range']))
    #         if not columns[k].get('nullable', False) and valdf[k].isnull().values.any():
    #             raise ValueError(
    #                 "Invalid data found in '{}'. The '{}' column is not allowed to have 'null' values.".format(
    #                     path.name, k))
    #         if 'match' in columns[k]:
    #             leftfile = self.__class__.query.filter(
    #                 self.__class__.name.endswith(columns[k]['match']['leftfile'], escape="*"),
    #                 self.__class__.campaign_id == self.campaign_id).one()
    #             leftdf = self.file2df(leftfile)
    #             if not valdf[k].isin(leftdf[columns[k]['match']['leftcol']]).all():
    #                 raise ValueError(
    #                     "There are values in the '{}' column of '{}' that do not match with the values in the '{}' column of '{}'.".format(
    #                         k, path.name, columns[k]['match']['leftcol'], leftfile.name))
    #
    #         elif k not in valdf.columns and columns[k].get('required', False):
    #             raise ValueError("Missing column in '{}'. Expected a column named '{}'.".format(path.name, k))
