import hashlib
import os

import pandas as pd
import time

import requests
from flask import make_response

from plugins.dataset import Platform, MEDIAINFO_FILE_PATTERN
from server import APIException

from plugins.ftp import SFTPClient

UPLOAD_OPTION_CFG_URL = "http://localhost:{settings[PORT]}/static/settings/globalarchive_upload_types.cfg.json"
FILE_VALIDATION_CFG_PATTERN = "{host_url}/static/json/filequery_validation.json"
SYNCSTAT_FILE_PATTERN = os.path.join("{path}",".syncstat.json")

CAMPAIGN_UI_GET_URL = "{host_url}/geodata/data/campaign/get/{id}"
CAMPAIGN_API_POST_URL = "{host_url}/api/campaign/save"
CAMPAIGN_API_PATCH_URL = "{host_url}/api/campaign/{id}/save"
CAMPAIGN_API_GET_URL = "{host_url}/api/campaign-full/{id}"

# TODO: these credentials should be passed down from the online platform using the API key
sftp_user = os.getenv("SYNCTOOL_SFTP_USER", "synctool")
sftp_pass = os.getenv("SYNCTOOL_SFTP_PASS", "highlysecure")
sftp_host = os.getenv("SYNCTOOL_SFTP_HOST", "144.6.225.14")

class GA(Platform):
    def __init__(self, *args, **kwargs):
        global UPLOAD_OPTION_CFG_URL, FILE_VALIDATION_CFG_PATTERN
        super().__init__(*args, **kwargs)
        self.upload_options = None
        self.validation_params = None
        self.file_validation_cfg_file = FILE_VALIDATION_CFG_PATTERN.format(settings = self.settings, host_url=self.host_url, api_key=self.api_key)

        self.sftp_client = SFTPClient(host_server=sftp_host, username=sftp_user, password=sftp_pass)

        # Update with settings (if appropriate)
        UPLOAD_OPTION_CFG_URL = UPLOAD_OPTION_CFG_URL.format(settings = self.settings, host_url=self.host_url, api_key=self.api_key)
        #FILE_VALIDATION_CFG_URL = FILE_VALIDATION_CFG_URL.format(settings = self.settings, host_url=self.host_url, api_key=self.api_key)


    def get_online_record_key(self):
        return str(hashlib.md5(self.host_url.encode('utf-8')).hexdigest())

    # def get_campaign_online(self, id=None, *args, **kwargs):
    #     endpoint = "/api/campaign-full/{}".format(id)
    #     data = self.get_json_api(endpoint)
    #     data["cloud_dataset_url"] = CLOUD_DATASET_URL.format(host_url=self.host_url, id=id)
    #     return data

    def get_online_record_syncstat(self, path, *args, **kwargs):
        key = self.get_online_record_key()
        try:
            return self.read_json(SYNCSTAT_FILE_PATTERN.format(path=path), field=key)
        except Exception as e:
            return dict(synced_at=None)

    def get_online_record_data(self, path, *args, **kwargs):
        status = self.get_online_record_syncstat(path)
        cmpdata = {}
        try:
            data = self.get_json_api(status.get("apiurl"), append_host=False) if "apiurl" in status else {}
            cmpdata = data.get("data", {})
        except APIException as e:
            if (e.code == 404):
                cmpdata = dict(error=True, message="The linked record could not be found. It is possible that it was deleted on the server or the link has broken.")
        #print(data)

        if cmpdata:
            cmpdata["_syncstat_path"] = SYNCSTAT_FILE_PATTERN.format(path=path)
            cmpdata["_online_url"] = CAMPAIGN_UI_GET_URL.format(host_url=self.host_url, id=status.get("id"))
            cmpdata["_path"] = path
        return cmpdata



    def sftp(self, method="status", *args, **kwargs):
        return getattr(self.sftp_client,method)(*args, **kwargs)

    def sync_campaign(self, path, *args, **kwargs):
        data = self.get_campaign(path)

        # Get syncstat
        syncstat = self.get_online_record_syncstat(path)
        campaign_id = syncstat.get("id", None)

        # build campaign field structure for API post
        cmp_name = data.get('name','')
        post_data = dict(
            campaign_name=cmp_name,
            campaign_description=data.get("description", ""),
            campaign_project=int(data.get("project",{}).get("id")), #id
            campaign_platform=int(data.get("platform",{}).get("id")), #id
            cmpinfo="[]"
        )

        # Assemble filed for API post
        file_data = []
        for f in data.get("metadata",[]):
            fpath = f.get("path")
            if os.path.isfile(fpath):
                fname = f"{cmp_name}{os.path.basename(fpath)}"
                file_data.append(('file', (fname, open(fpath, 'rb'))))

        # Perform API post/patch
        if campaign_id is not None:
            patch_url = CAMPAIGN_API_PATCH_URL.format(host_url=self.host_url, id=campaign_id)
            r = requests.post(patch_url, data=post_data, files=file_data, headers={"auth-token": self.api_key})
        else:
            post_url = CAMPAIGN_API_POST_URL.format(host_url=self.host_url)
            r = requests.post(post_url, data=post_data, files=file_data, headers={"auth-token": self.api_key})

        # Check API post response and save results
        if r.ok:
            response = r.json()
            if response.get("status") == "ok":
                id = response.get("msg")
                apiurl = CAMPAIGN_API_GET_URL.format(host_url=self.host_url, id=id)
                uiurl = CAMPAIGN_UI_GET_URL.format(host_url=self.host_url, id=id)
                key = self.get_online_record_key()
                cmp_data = dict(apiurl=apiurl, id=id, synced_at=time.time(), campaign_name=cmp_name, uiurl=uiurl)
                self.write_json(SYNCSTAT_FILE_PATTERN.format(path=path), data=cmp_data, field=key)
                return dict(campaign_id=response.get("msg"))
            else:
                raise APIException(response.get("msg"), code=r.status_code)
        else:
            raise APIException("Unable to sync campaign: {} (reason: {})".format(cmp_name, r.reason), code=r.status_code)



    def get_campaign_upload_types(self, *args, **kwargs):
        return [i["name"] for i in self._get_campaign_upload_options()]

    def import_directory(self, upload_type=None, directory=None, *args, **kwargs):
        params = self._get_campaign_upload_options()
        allowed_upload_types = self.get_campaign_upload_types()
        upload_data = None

        if upload_type is not None and upload_type in allowed_upload_types:
            upload_data = list(filter(lambda i: i["name"] == upload_type, params))[0]
            self.match_files(upload_data.get("metadata", []), directory)
            self.match_files(upload_data.get("media", []), directory)

        return dict(data=upload_data, selected_type=upload_type, selected_directory=directory)

    def get_platforms(self, *args, **kwargs):
        endpoint='/api/platform?results_per_page=100&q={"order_by":[{"field":"name","direction":"asc"}]}'
        return self.get_json_api(endpoint).get("data")

    def get_projects(self, *args, **kwargs):
        self.assert_logged_in()
        endpoint = '/api/groups'
        qs = 'q={"filters":[{"name":"grouptype","op":"eq","val":"project"},' \
             '{"name":"owner_user","op":"has","val":{"name":"_api_token","op":"eq","val":"%s"}}],' \
             '"order_by":[{"field":"group_created_at","direction":"desc"}]}&results_per_page=100' % self.api_key

        return self.get_json_api(endpoint, qs=qs).get("data")

    def create_media_list(self, *args, **kwargs):
        pass

    def get_em_skeleton_file(self, path, *args, **kwargs):
        """
        NOTE: this method returns a Flask response directly, so response does not need to be jsonified
        :param path:
        :return:
        """
        mediainfo_file_path = MEDIAINFO_FILE_PATTERN.format(project_path=path)
        mediainfo_df = self.file2df(mediainfo_file_path)
        emskeleton_df = pd.DataFrame(columns=["OpCode"])  # create empty dataframe
        emskeleton_df["OpCode"] = mediainfo_df["Sample"].unique()
        resp = make_response(emskeleton_df.to_csv(index=False))
        resp.headers["Content-Disposition"] = "attachment; filename=EM_skeleton_file.csv"
        resp.headers["Content-Type"] = "text/csv"
        return resp

    def preprocess_campaign(self, data, *args, **kwargs):
        data["messages"] = []
        file_list = data.get("metadata", [])
        mediainfo_file_path = MEDIAINFO_FILE_PATTERN.format(project_path=data.get("path"))
        metadata_file_path = self._get_datafile_from_pattern(file_list, "*_Metadata.csv").get("path", None)  # get the expected path
        movieseq_file_path = self._get_datafile_from_pattern(file_list, "*_MovieSeq.txt").get("path", None)  # get the expected path

        # open movieseq file , if valid
        moviseq_df = self.file2df(movieseq_file_path) if movieseq_file_path and os.path.isfile(movieseq_file_path) else None
        metadata_df = None

        # Auto create metadata file if it does not exist
        if metadata_file_path and not os.path.isfile(metadata_file_path):
            # TODO: do not make this dependent on internet connection, rather use local definition for metadata template
            metadata_cols = self.get_validation_params().get("*_Metadata.csv").keys()      # get columns
            metadata_df = pd.DataFrame(columns=metadata_cols)                  # create empty dataframe
            if moviseq_df is not None and not moviseq_df.empty:
                metadata_df["Sample"] = moviseq_df["OpCode"].unique()          # prepopulate opcodes to sample columns
            metadata_df.to_csv(metadata_file_path,index=False)                 # save to disk
            data["messages"].append("Automatically created empty _Metadata.csv file and attempted pre-filling")
        elif metadata_file_path:
            metadata_df = self.file2df(metadata_file_path)

        # Create / update media file
        try:
            if metadata_df is not None and moviseq_df is not None:
                mediainfo_df = pd.merge(moviseq_df.rename(columns={"OpCode":"Sample", "Filename": "filename"}), metadata_df, on="Sample", how="inner")
                mediainfo_df.to_csv(mediainfo_file_path, index=False)
            elif metadata_df is not None and moviseq_df is None:
                metadata_df["filename"] = metadata_df["Sample"]+".mp4"
                metadata_df.to_csv(mediainfo_file_path, index=False)
        except Exception as e:
            if os.path.isfile(mediainfo_file_path):
                self.fileop(path=mediainfo_file_path, op="rm")
            print(f"\n\nUnable to create metadata-media file: {mediainfo_file_path}\n\n")



    def get_user(self, email=None, *args, **kwargs):
        endpoint = "/api/users"
        if email is not None:
            qs = 'q={"filters":[{"name":"email","op":"eq","val":"%s"}],"single":true}' % email
        else:
            assert self.api_key, "API KEY has not been set"
            qs = 'q={"filters":[{"name":"_api_token","op":"eq","val":"%s"}],"single":true}' % self.api_key
        user = self.get_json_api(endpoint, qs=qs).get("data")
        self.user = user if user else None
        return self.user


    ###################################################################################################################
    # Private methods
    ###################################################################################################################
    def get_validation_params(self, refresh=False, pattern=None, *args, **kwargs):
        try:
            if self.validation_params is None or refresh:
                self.validation_params = self.get_json_api(self.file_validation_cfg_file, append_host=False).get("data")
            #print(self.validation_params)
            return self.validation_params if pattern is None else self.validation_params.get(pattern,{})
        except Exception as e:
            return {}

    def _get_campaign_upload_options(self, refresh=False, *args, **kwargs):
        try:
            if self.upload_options is None or refresh:
                print(f"Upload settings URL: {UPLOAD_OPTION_CFG_URL}")
                self.upload_options = self.get_json_api(UPLOAD_OPTION_CFG_URL, append_host=False).get("data")
            return self.upload_options
        except Exception as e:
            return []


