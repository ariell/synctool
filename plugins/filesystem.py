# import csv
import fnmatch
import os
import glob
import json
# import humanize
import shutil
# import stat
import traceback
from datetime import datetime
from shutil import copyfile
from parse import parse

from pandas import read_csv, np

if os.name == 'nt':
    import win32api, win32con

OPERATORS = {
    # Operators which accept a single argument.
    'is_null': lambda f: f == None,
    'is_not_null': lambda f: f != None,
    'is_true': lambda f: f,
    'is_false': lambda f: ~f,
    # Operators which accept two arguments.
    '==': lambda f, a: f == a,
    'eq': lambda f, a: f == a,
    'equals': lambda f, a: f == a,
    'equal_to': lambda f, a: f == a,
    '!=': lambda f, a: f != a,
    'ne': lambda f, a: f != a,
    'neq': lambda f, a: f != a,
    'not_equal_to': lambda f, a: f != a,
    'does_not_equal': lambda f, a: f != a,
    '>': lambda f, a: f > a,
    'gt': lambda f, a: f > a,
    '<': lambda f, a: f < a,
    'lt': lambda f, a: f < a,
    '>=': lambda f, a: f >= a,
    'ge': lambda f, a: f >= a,
    'gte': lambda f, a: f >= a,
    'geq': lambda f, a: f >= a,
    '<=': lambda f, a: f <= a,
    'le': lambda f, a: f <= a,
    'lte': lambda f, a: f <= a,
    'leq': lambda f, a: f <= a,
    'like': lambda f, a: a in f,
}

# File operation constants
FILEOP_CP = "cp"
FILEOP_MV = "mv"
FILEOP_LN = "ln"
FILEOP_MKDIR = "mkdir"
#FILEOP_LNDIRFILES = "lndirfiles"
FILEOP_RMTREE = "rmtree"
FILEOP_RM = "rm"

#LINKDIR_FILE = ".islinkeddir"
LINKFILE_FMT = "[LINKFILE]{path}"
IGNORE_FILES = ["Thumbs.db", ".DS_Store"]

class FileSystem:
    def __init__(self, *args, editable_files=[], editable_dirs=[], **kwargs):
        self.editable_files = editable_files
        self.editable_dirs = editable_dirs

    def browse(self, path, filters=[], inspect_function=None, *args, **kwags):
        path = os.path.abspath(path)
        files = []
        filters = filters if isinstance(filters, (list, tuple)) else json.loads(filters) if filters else []
        disjunction = all  # all | any

        for f in glob.glob(path):
            try:
                fstats = os.stat(f)
                finfo = {
                    "path": get_file_path(f),
                    "basename": os.path.basename(f),
                    # "size": humanize.naturalsize(fstats.st_size),
                    "size": fstats.st_size,
                    "ctime": datetime.fromtimestamp(fstats.st_ctime).strftime('%Y-%m-%d %H:%M:%S'),
                    "atime": datetime.fromtimestamp(fstats.st_atime).strftime('%Y-%m-%d %H:%M:%S'),
                    "mtime": datetime.fromtimestamp(fstats.st_mtime).strftime('%Y-%m-%d %H:%M:%S'),
                    "type": "dir" if os.path.isdir(f) else os.path.splitext(f)[1][1:]
                }

                if inspect_function is not None and callable(inspect_function):
                    inspect_function(finfo)

                filter_result = []
                for fil in filters:
                    opfunc = OPERATORS[fil["op"]]
                    filter_result.append(opfunc(finfo[fil["field"]], fil["val"]))
                if disjunction(filter_result):
                    files.append(finfo)
            except Exception as e:
                files.append({"path": f, "type": "ERROR: {}".format(e)})

        return files

    def dirstats(self, path, *args, **kwargs):
        #print (os.listdir(path))
        return {"ndirs": sum(os.path.isdir(os.path.join(path,i)) for i in os.listdir(path))}

    def fileop(self, path=None, dest=None, op=None, *arg, **kwargs):

        if path is None:
            raise ValueError("File path can not be null")
        try:
            # Check if dest is directory, and if so, assume same file name
            if dest is not None and os.path.isdir(dest):
                dest = os.path.join(dest, os.path.basename(path))

            # Do operation
            if path != dest and path:                        # do nothing if path == dest
                if op == FILEOP_MKDIR:         # create dir if doesn't exist
                    if not os.path.isdir(path):
                        os.makedirs(path)
                elif op == FILEOP_MV:
                    os.rename(path, dest)
                elif op == FILEOP_LN:
                    #dest = dest+FILELINK_EXT
                    if os.path.isfile(dest):        # remove/overwrite link if already exists
                        os.remove(dest)
                    #os.symlink(path, dest)
                    with open(dest, 'w') as f: f.write(LINKFILE_FMT.format(path=path))
                    #path_stat = os.stat(path)                                   # Get file stats
                    #os.utime(dest, (path_stat.st_atime, path_stat.st_mtime), follow_symlinks = False)    # ensure atime and mtime are the same as the source
                elif op == FILEOP_CP:
                    copyfile(path, dest)
                    path_stat = os.stat(path)                                   # Get file stats
                    os.utime(dest, (path_stat.st_atime, path_stat.st_mtime))    # ensure atime and mtime are the same as the source
                elif op == FILEOP_RMTREE:
                    shutil.rmtree(path)
                elif op == FILEOP_RM:
                    os.remove(path)
                else:
                    raise ValueError("Unrecognised file operation: {}".format(op))
                return {"op": op, "success": True, "path": path, "dest": dest}
            return {"op": op, "success": False, "path": path, "dest": dest}     # return success=False without raising exception
        except Exception as e:
            raise Exception("Unable to complete file operation. op:'{}', path:'{}', dest:'{}'. {}".format(op, path, dest, e))


    def read_json(self, jsonfile, field=None, *args, **kwargs):
        # return output
        with open(jsonfile) as config_json:
            jsondata = json.load(config_json)
            if type(field) == str:
                return jsondata[field]
            elif type(field) == list:
                return {f: jsondata[f] for f in field}
            return jsondata

    def write_json(self, jsonfile, data=None, field=None, append_list=False, *args, **kwargs):
        # if data is provided, write to file
        if data is not None:
            if field is not None:
                json_new = self.read_json(jsonfile) if os.path.isfile(jsonfile) else {}
                if append_list is True:
                    if field in json_new:
                        json_new[field].append(data)    # append to list
                    else:
                        json_new[field] = [data]    # add list
                else:
                    json_new[field] = data          # replace field
            else:
                json_new = data
            open(jsonfile, 'w').write(json.dumps(json_new, indent=4))
        return self.read_json(jsonfile, field=field)

    def in_directory(self, filepath, directory, *args, **kwargs):
        """
        Check that a file is contained within a directory
        :param filepath:
        :param directory:
        :return:
        """
        #make both absolute
        directory = os.path.join(os.path.realpath(directory), '')
        filepath = os.path.realpath(filepath)
        return os.path.commonprefix([filepath, directory]) == directory

    def file_is_writable(self, filepath, allowed_dirs, allowed_files, *args, **kwargs):
        filepath = os.path.realpath(filepath)
        for f in allowed_files:
            if os.path.realpath(f) == filepath:
                return True
        for d in allowed_dirs:
            if self.in_directory(filepath, d):
                return True
        return False

    def file2df(self, path, sep=None):
        try:
            if sep is None:
                sep = "," if path.strip().endswith(".csv") else "\t"
            return read_csv(path, sep=sep, parse_dates=False, index_col=False)
        except IndexError as e:
            raise IndexError("Index error: check all columns have unique headings.")


    def match_media_metadata_to_dir(self, mediainfo_file_path, dirpath, extended=True, *args, **kwargs):
        errors = []
        warnings = []


        if os.path.isfile(mediainfo_file_path):
            # flink_nchars = len(FILELINK_EXT)
            campaign_path = os.path.dirname(mediainfo_file_path)
            mediainfo_df = self.file2df(mediainfo_file_path)
            media_file_list = [f for f in next(os.walk(dirpath))[2] if not is_hidden_file(f)]
            # media_file_list = [(fn[0:-flink_nchars] if fn.endswith(FILELINK_EXT) else fn) for fn in media_file_list_links]
            n_files = len(media_file_list)
            mediainfo_list = list(mediainfo_df["filename"])
            mismatch_in_file = list(set(mediainfo_list).difference(media_file_list))
            mismatch_in_dir = list(set(media_file_list).difference(mediainfo_list))

            n_matches = len(set(mediainfo_list).intersection(media_file_list))
            n_mismatches_in_file = len(mismatch_in_file)
            n_mismatches_in_dir = len(mismatch_in_dir)
            if n_mismatches_in_file > 0:
                errors.append(f"There are {n_mismatches_in_file} media entries in the data file list that have not "
                              f"been linked to files. This could result in dead links in the metadata records.")
            if n_mismatches_in_dir > 0:
                errors.append(f"There are {n_mismatches_in_dir} linked media files in the media directory that do not "
                              f"have any metadata. This could lead to uploading orphaned files without associated metadata.")

            data = dict(errors=errors, warnings=warnings, n_files=n_files, n_matches=n_matches, campaign_path=campaign_path,
                        n_mismatches_in_file=n_mismatches_in_file, n_mismatches_in_dir=n_mismatches_in_dir)

            if extended is True:
                #data['mismatch_in_file'] = mismatch_in_file
                data['media_file'] = mediainfo_file_path
                data['media_dir'] = dirpath
                data['mismatch_in_dir'] = [
                    dict(filename=f, path=os.path.join(dirpath, f), target_path=get_file_path(os.path.join(dirpath, f)))
                    for f in mismatch_in_dir
                ]
                # data['mismatch_in_dir'] = [dict(filename=f, path=os.path.join(dirpath,f)) for f in mismatch_in_dir]
                for index, row in mediainfo_df.iterrows():
                    mediainfo_df.loc[index, '_is_linked'] = mediainfo_df.loc[index, 'filename'] in media_file_list
                    mediainfo_df.loc[index, '_path'] = os.path.join(dirpath, mediainfo_df.loc[index, 'filename'])
                    mediainfo_df.loc[index, '_target_path'] = get_file_path(os.path.join(dirpath, mediainfo_df.loc[index, 'filename']))
                mediainfo_df.sort_values(by=["_is_linked"], ascending=True, inplace=True)   # sort results
                data["data"] = mediainfo_df.to_dict(orient="records")


            # print(dirpath)
            # print(data)
        else:
            #raise FileNotFoundError(f"File not found: {mediainfo_file_path}")
            data = dict(errors=["The media-metadata file could not be found. This file is normally generated "+
                               "automatically, but could not for this dataset. It may be due to a bad metadata file."],
                        warnings=[], n_files=0, n_matches=0,
                        n_mismatches_in_file=0, n_mismatches_in_dir=0)

        return data


    def get_matched_column(self, fpath, match_val_params, *args, **kwargs):
        leftfile_pattern = match_val_params.get('leftfile')
        leftfile_col = match_val_params.get('leftcol')
        file_list = glob.glob(os.path.join(os.path.dirname(fpath), leftfile_pattern))
        assert len(file_list) > 0, "No {} file found / file missing.".format(leftfile_pattern)
        leftfile_path = file_list[0]
        leftdf = self.file2df(leftfile_path)
        return dict(data=list(leftdf[leftfile_col]), pattern=leftfile_pattern, col=leftfile_col, path=leftfile_path)


    def validate_file(self, fpath, val_params, file_list=None, *args, **kwargs):
        #print(file_list)
        errors = []
        warnings = []

        if val_params:
            if fpath is None or not os.path.isfile(fpath):
                errors.append("{} does not appear to be a valid file.".format(fpath))
            else:
                try:
                    valdf = self.file2df(fpath)
                except Exception as e:
                    errors.append(f"Unable to open file: {e} ({fpath})")
                    traceback.print_exc()
                else:
                    try:
                        nrows = valdf.shape[0]

                        for col_key in val_params.keys():
                            if col_key in valdf.columns and nrows > 0:
                                if 'type' in val_params[col_key]:
                                    if val_params[col_key]['type'] == 'float' and valdf[col_key].dtype != np.float64:
                                        errors.append("The '{}' column needs to be of type: 'float'".format(col_key))
                                    if (val_params[col_key]['type'] == 'int' or val_params[col_key]['type'] == 'integer') and valdf[col_key].dtype != np.int64:
                                        errors.append("The '{}' column needs to be of type: 'integer'.".format(col_key))
                                    if val_params[col_key]['type'] == 'number' and valdf[col_key].dtype != np.number and valdf[
                                        col_key].dtype != np.int64 and valdf[col_key].dtype != np.float64:
                                        errors.append("The '{}' column needs to be of type: 'number'.".format(col_key))
                                if 'format' in val_params[col_key] and val_params[col_key]['type'] == 'datetime':  # TODO: this should be regex for flexibility
                                    try:
                                        valdf[col_key].apply(lambda x: datetime.strptime(str(x), val_params[col_key]['format']))
                                    except Exception as e:
                                        errors.append("The '{}' column needs to be: 'datetime' with format '{}'.".format(col_key, val_params[col_key]['format']))
                                if 'isini' in val_params[col_key] and not valdf[col_key].astype(str).str.lower().isin(
                                        [str(x).lower() for x in val_params[col_key]['isini']]).all():
                                    errors.append("The '{}' column must be one of: {}.".format(col_key, ", ".join(val_params[col_key]['isini'])))
                                if 'isin' in val_params[col_key] and not valdf[col_key].isin(val_params[col_key]['isin']).all():
                                    errors.append("The '{}' column must be one of: {} (case sensitive).".format(col_key,", ".join(val_params[col_key]['isin'])))
                                if val_params[col_key].get('isunique', False) and valdf[col_key].duplicated().any():
                                    errors.append("Duplicate: The '{}' column must be unique.".format(col_key))
                                if 'range' in val_params[col_key] and (
                                        valdf[col_key].dtype != np.number or not valdf[col_key].between(
                                        *val_params[col_key]['range']).all()):
                                    errors.append("The '{}' column must have values between '{}' and '{}'.".format(col_key, *
                                    val_params[col_key]['range']))
                                if not val_params[col_key].get('nullable', False) and valdf[col_key].isnull().values.any():
                                    errors.append("The '{}' column is not allowed to have 'null' values.".format(col_key))
                                if 'match' in val_params[col_key]:
                                    try:
                                        matched_file = self.get_matched_column(fpath, val_params[col_key]['match'])
                                        not_in = ~valdf[col_key].isin(matched_file.get("data"))
                                        if not_in.sum() > 0:
                                            errors.append(
                                                "There are {} values in the '{}' column that do not match with the values in the '{}' column of '{}'.".format(
                                                    not_in.sum(), col_key, matched_file.get("col"), matched_file.get("pattern")))
                                    except Exception as e:
                                        errors.append("Unable to match the '{0}' column to the '{1}' column of '{2}'. {3}".format(
                                                col_key, val_params[col_key]['match']['leftcol'], val_params[col_key]['match']['leftfile'], e))

                            elif col_key not in valdf.columns and val_params[col_key].get('required', False):
                                errors.append("Missing required column: '{}'.".format(col_key))
                    except Exception as e:
                        errors.append(f"Unable to validate file: {e}")
                        traceback.print_exc()

        return dict(errors=errors, warnings=warnings)

    def _get_datafile_from_pattern(self, file_list, pattern, *args, **kwargs):
        for f in file_list:
            if fnmatch.fnmatch(f.get("path",""), pattern):
                return f
        return {}



# def get_linked_filepath(path):
#     if not os.path.isfile(path):
#         return None
#     try:
#         with open(path, encoding='utf8') as f:
#             path = f.read()
#     except:
#         pass
#     return path


def get_file_path(path):
    if os.path.isdir(path):         # return dir path if dir
        return path
    elif not os.path.isfile(path):  # return non if not file or dir
        return None
    try:                            # check if linkfile by parsing 1st line of file to see if link format
        # TODO: add a FAST check to determine if binary or text file and only check text files.
        with open(path, 'r', encoding='utf8') as f:
            l = f.readline()
        p = parse(LINKFILE_FMT, l)
        if p:
            path = p.named.get("path")
            #print(f"FOUND LINK TARGET: {path}")
        # else:
        #     print(f"NOT LINKED FILE: {path}")
    except Exception as e:
        print(f"ERROR CHECKING PATH LINK: {e}")
    return path

def is_hidden_file(path):
    try:
        if os.name == 'nt':
            attribute = win32api.GetFileAttributes(path)
            return attribute & (win32con.FILE_ATTRIBUTE_HIDDEN | win32con.FILE_ATTRIBUTE_SYSTEM) or path.startswith(('.', '$'))
    except:
        pass
    return path.startswith(('.', '$')) or os.path.basename(path) in IGNORE_FILES