import hashlib
import threading
from getpass import getpass
from time import time
# from uuid import uuid4, UUID

import paramiko
import os

import sys
from humanize import naturalsize
import json
# from .filesystem import LINKDIR_FILE, get_linked_filepath, is_hidden_file  #
from plugins.filesystem import get_file_path, is_hidden_file

MAX_TRANSFERS = 10
TFSTAT_FILE_PATTERN = os.path.join("{source}",".tfstat.json")
TFSTAT_WRITE_PERIOD = 5   # seconds between writes to TFSTAT_FILE

DEBUG = False

class SFTPClient:
    _transport = None
    _tfinfo = {}

    def __init__(self, host_server=None, port=22, username=None, password=None, basedir="staging/"):
    # def __init__(self):
        self.host_server = host_server
        self.port = port
        self.username = username
        self.password = password
        self.basedir = basedir

    def update(self, host_server=None, port=22, username=None, password=None, basedir="staging/"):
        if host_server is not None:
            self.host_server = host_server
        if port is not None:
            self.port = port
        if username is not None:
            self.username = username
        if password is not None:
            self.password = password
        if basedir is not None:
            self.basedir = basedir

    def status(self, tfid=None, source=None, *args, **kwargs):
        if tfid is None and source is None:                             # list all active tfs if no args
            return [self.status(tfid=i) for i in self._tfinfo.keys()]
        elif tfid is None and source is not None:
            tfid = TFStatus.get_create_uuid(source)                     # if source, infer tfid

        if tfid in self._tfinfo:                                        # active tf, retrieve realtime status
            data = self._tfinfo[tfid].get("tfstat").get_status()
        else:                                                           # not active, retrieve cached stat from source/record
            data = TFStatus.get_status_from_source(source)
        return data

    def connect(self, host_server=None, port=22, username=None, password=None, *args, **kwargs):
        assert host_server is not None, "Invalid host_server"
        assert username is not None, "Invalid username"
        assert password is not None, "Invalid password"
        print(f"Opening connection to {host_server}...")
        self._transport = paramiko.Transport(host_server, port)
        self._transport.connect(username=username, password=password)
        #self._sftp = paramiko.SFTPClient.from_transport(self._transport)

    def stop(self, tfid=None, *args, **kwargs):
        assert tfid in self._tfinfo, f"Cannot stop transfer with ID: '{tfid}'. No active transfer found."
        self._tfinfo[tfid].get("sftp").close()
        self._tfinfo.pop(tfid)
        return self.status(tfid=tfid)

    def disconnect(self):
        print("Closing connection...")
        #self._sftp.close()
        self._transport.close()

    def upload(self, source, *args, **kwargs):
        # Initialise things
        if not self.is_connected():
            self.connect(host_server=self.host_server, username=self.username, password=self.password, port=self.port)
        tfstat = TFStatus(source)
        sftp = paramiko.SFTPClient.from_transport(self._transport)

        # Start transfer thread
        p = threading.Thread(target=self._upload_thread, args=(source,), kwargs={"sftp": sftp, "tfstat": tfstat})
        p.daemon = True
        p.start()

        # return status
        return self.status(tfid=tfstat.tfid)

    def _upload_thread(self, source, sftp, tfstat, *args, **kwargs):
        if tfstat.tfid in self._tfinfo:                                 # if already registered, stop and restart
            self.stop(tfid=tfstat.tfid)
        self._tfinfo[tfstat.tfid] = dict(tfstat=tfstat, sftp=sftp)      # register handles
        self.put_dir(source, sftp=sftp, tfstat=tfstat)                  # do transfer
        sftp.close()                                                    # close connection
        tfstat.finalise()                                               # finalise and cleanup
        self._tfinfo.pop(tfstat.tfid)                                   # remove from register

    def is_connected(self):
        if self._transport is None:
            return False
        else:
            try:
                self._transport.send_ignore()
            except Exception as e:
                return False
            return self._transport.is_active()


    def put_dir(self, source, target=None, skip_existing=True, tfstat=None, sftp=None):
        """
        Uploads the contents of the source directory to the target path. The
        target directory needs to exists. All subdirectories in source are
        created under target.
        skip_existing==True, checks size and mtime of files and only upload if they do not match (i.e.: it will sync).
        """
        assert source is not None and os.path.isdir(source), "Source needs to be a valid directory or a list of objects!"

        # check if tfstat has been instantiated, and if not, create it
        if tfstat is None:
            tfstat = TFStatus(source)
        if sftp is None:
            sftp = paramiko.SFTPClient.from_transport(self._transport)
        if target is None:
            target = os.path.join(self.basedir, os.path.basename(os.path.realpath(source)))

        try:
            filelist = os.listdir(source)
            self.mkdir(target, ignore_existing=True, sftp=sftp)                # ensure target directory exists on server
            tfstat.add_dir(nitems=len(filelist))                    # add to total item count

            # check if file contents are linked by looking for presence of special file
            #is_linked = os.path.isfile(os.path.join(source, LINKDIR_FILE))

            for item in filelist:
                fpath = os.path.join(source, item)
                if os.path.isfile(fpath):
                    if not is_hidden_file(item):  # and item != LINKDIR_FILE:  # if not hidden file and not special link file
                        # if is_linked:  # check if file contents are linked, and if so, follow links to upload
                        #     print(f"Getting link for: {fpath}")
                        #     fpath = get_linked_filepath(fpath)
                        fpath = get_file_path(fpath)       # get actual or linked file path
                        local_stat = os.stat(fpath)        # check local file stats
                        if skip_existing is True:
                            try:
                                remote_stat = sftp.stat(f"{target}/{item}")
                                skip = int(remote_stat.st_size) == int(local_stat.st_size) \
                                         and int(remote_stat.st_mtime) >= int(local_stat.st_mtime)  # skip if size and mtime are the same
                            except IOError as e:
                                skip = False                                                        # do put if file does not exist
                        else:
                            skip = False                                                            # do put if skip_existing is set to False

                        if not skip:
                            # put the file and manually set times so that they match for future syncing check
                            sftp.put(fpath, f"{target}/{item}", callback=tfstat.progress_callback, confirm=True)
                            sftp.utime(f"{target}/{item}", (local_stat.st_atime, local_stat.st_mtime))
                        tfstat.add_file(fname=fpath, skip=skip)
                elif os.path.isdir(fpath):
                    # self.mkdir('%s/%s' % (target, item), ignore_existing=True)
                    self.put_dir(fpath, f"{target}/{item}", tfstat=tfstat, sftp=sftp)
        except Exception as e:
            tfstat.error = str(e)       # add error message to status
            tfstat.save_status()        # log error in tf record
            raise e

        return sftp


    def mkdir(self, path, mode=511, ignore_existing=False, sftp=None):
        """
        Augments mkdir by adding an option to not fail if the folder exists
        """
        try:
            sftp.mkdir(path, mode)
            # self.created_directory_count += 1  # add to created directory count
        except IOError as e:
            if ignore_existing:
                pass
            else:
                raise e


class TFStatus:
    def __init__(self, source, tfid=None):
        self.tfid = tfid or self.get_create_uuid(source)      # generate unique ID for this transfer
        self.total_nitems = 0
        self.cur_file_size = 0
        self.cur_file_tfd = 0
        self.ndirs = -1                             # don't count root-level directory
        self.nfiles_tfd = 0
        self.nfiles_skipd = 0
        self.total_size = 0
        self.last_file = None
        self.started_at = time()
        self.updated_at = time()
        self.source = source
        self.error = None
        self.is_complete = False

        # save status file for later reference
        self.save_status()

    def add_file(self, fname=None, skip=False):
        self.last_file = fname
        self.updated_at = time()                    # update with latest time
        if skip:
            self.nfiles_skipd += 1                  # add to skipped file count
        else:
            self.nfiles_tfd += 1                    # add to transferred count
            self.total_size += self.cur_file_size   # add to transferred size

    def add_dir(self, nitems=0):
        self.ndirs += 1                             # increment counter for number of directories (if subdir)
        self.total_nitems += nitems                 # add to total item count
        self.updated_at = time()                    # update with latest time

    def progress_callback(self, transferred, toBeTransferred):
        self.cur_file_size = toBeTransferred
        self.cur_file_tfd = transferred
        self.updated_at = time()                    # update with latest time
        if DEBUG:
            self.print_status()

    def cur_file_prog(self):
        return self.cur_file_tfd / self.cur_file_size if self.cur_file_size>0 else 0

    def total_prog(self):
        return (self.ndirs + self.nfiles_tfd + self.nfiles_skipd) / self.total_nitems if self.total_nitems>0 else 0

    def duration(self):
        return self.updated_at - self.started_at

    def save_status(self):
        status_json = json.dumps(self.get_status(), indent=2)
        if status_json:
            with open(TFSTAT_FILE_PATTERN.format(source=self.source), 'w') as fp:
                print(status_json, file=fp)
                #json.dump(self.get_status(), fp)

    def get_status(self):
        info = self.__dict__.copy()
        info["cur_file_prog"] = self.cur_file_prog()
        info["total_prog"] = self.total_prog()
        info["duration"] = self.duration()
        info["is_active"] = not self.error
        return info

    def print_status(self, end="\r"):
        print(f"{round(self.total_prog()*100,1)}% "
              f"({self.ndirs + self.nfiles_tfd + self.nfiles_skipd}/{self.total_nitems}) | "
              f"{round(self.cur_file_prog()*100,1)}% "
              f"({naturalsize(self.cur_file_tfd)}/{naturalsize(self.cur_file_size)}) | "
              f"{self.duration()} s       ",
              end=end)

    def finalise(self):
        self.is_complete = True
        self.save_status()
        print(f"Completed | Transferred: {naturalsize(self.total_size)} | "
              f"synced {self.total_nitems} items: {self.ndirs} directories, "
              f"{self.nfiles_tfd+self.nfiles_skipd} files "
              f"({self.nfiles_tfd} uploaded, {self.nfiles_skipd} skipped) in {self.duration()} s")

    @staticmethod
    def get_create_uuid(source):
        return hashlib.md5(source.strip("/").encode('utf-8')).hexdigest()
        # uuid_string = os.path.basename(source.strip(os.path.sep))
        # try:
        #     UUID(uuid_string, version=4)
        #     return uuid_string
        # except Exception:
        #     return str(uuid4())

    @staticmethod
    def get_status_from_source(source):
        tfstat_file = TFSTAT_FILE_PATTERN.format(source=source)
        if os.path.isfile(tfstat_file):
            try:
                with open(tfstat_file) as fp:
                    data = json.load(fp)
                    data["is_active"] = False
                    return data
            except:
                pass
        return dict(is_complete=False, is_active=False, started_at=None)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--local_dir", action='append', help="local directory")
    parser.add_argument("-u", "--username", help="username", default="tester")
    parser.add_argument("-s", "--host_server", help="server address/ip", default="144.6.225.14")
    parser.add_argument("-p", "--port", help="server port", type=int, default=22)

    args = parser.parse_args()

    if args.local_dir:
        prompt = f'Password for ({args.username}@{args.host_server}) ?: '
        password = getpass(prompt) if os.isatty(sys.stdin.fileno()) else sys.stdin.readline().rstrip()

        DEBUG = True

        sftp = SFTPClient(host_server=args.host_server, port=args.port, username=args.username, password=password)
        for l in args.local_dir:
            sftp.upload(l)
