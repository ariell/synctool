"""Synctool Server Module

This script can be invoked by itself and will spin up a flask web server which can be accessed through a browser on the
local machine.

"""
from __future__ import print_function

import json
import os
import time
import traceback
import datetime

import requests
import sys

from humanize import naturalsize
from jinja2.exceptions import TemplateNotFound
import jinja2
from importlib import import_module
from flask import Flask, request, jsonify, render_template
from flask.wrappers import Response as flask_response_type
from urllib.parse import quote_plus
from requests.exceptions import ConnectionError
#from gevent.pywsgi import WSGIServer
#from gevent.pywsgi import WSGIServer
#from gevent import monkey

# need to patch sockets to make requests async
#monkey.patch_all()
    
#######################################################################################################################
# Initialise server
#######################################################################################################################
app = Flask(__name__)  #, template_folder="templates")
#app.debug = True

#######################################################################################################################
# Load config files
#######################################################################################################################
if getattr( sys, 'frozen', False ):                             # check if frozen app
    default_config_object = "config.settings.FrozenAppConfig"   # use frozen config
else:
    default_config_object = "config.settings.DevConfig"         # use dev config if not frozen

# load default config fom object
app.config.from_object(os.environ.get("SYNCTOOL_CONFIG_OBJECT", default_config_object))  # optionally override config with environment variable
default_config_params = dict(app.config)                # store default config for comparative purposes

# Custom template directory
template_loader = jinja2.ChoiceLoader([
    app.jinja_loader,
    jinja2.FileSystemLoader(os.path.join(app.config.get("EXECUTABLE_PATH"),'templates')),
])
app.jinja_loader = template_loader
app.static_folder = os.path.join(app.config.get("EXECUTABLE_PATH"),"static")


#######################################################################################################################
# Some globals
#######################################################################################################################
application_plugins = {}
current_platform_plugin = None
logged_in_user = None

#######################################################################################################################
# Custom exception classes
#######################################################################################################################
class APIException(Exception):
    def __init__(self, msg, code=500):
        self.code = code
        super().__init__(msg)

# Custom template filters
app.jinja_env.filters['urlencode'] = lambda u: quote_plus(u) if u else ""
app.jinja_env.filters['topjson'] = lambda u: topjson(u) if u else ""
app.jinja_env.filters['naturalsize'] = lambda u: naturalsize(u) if u else ""
app.jinja_env.filters['basename'] = lambda u: os.path.basename(u) if u else ""
app.jinja_env.filters['datetime'] = lambda u, f: datetime.datetime.fromtimestamp(u).strftime(f) if u else ""


#######################################################################################################################
# Application server endpoints
#######################################################################################################################
@app.route("/", methods=["GET"])
def home():
    """

    :return:
    """
    params = request.args.to_dict()  # deployment_id, platform_id, period
    return render_template("synctool/main_ui.html", params=params, **get_template_params())

@app.route("/platform/set", methods=["GET"])
def set_platform():
    """

    :return:
    """
    global current_platform_plugin, logged_in_user
    old_platform_plugin = current_platform_plugin
    current_platform_plugin = request.args.get("platform_plugin")
    application_plugins["current_platform_plugin"] = application_plugins[current_platform_plugin]
    try:
        logged_in_user = application_plugins["current_platform_plugin"].get_user()
    except:
        logged_in_user = None
    return jsonify(old_platform_plugin=old_platform_plugin,
                   platform_plugin_data=app.config.get("PLATFORM_PLUGINS").get(current_platform_plugin),
                   **get_template_params())

@app.route("/ui/<path:template>", methods=["GET"])
def ui(template):
    """

    :param template:
    :return:
    """
    global current_platform_plugin
    kwargs = request.args.to_dict()  # deployment_id, platform_id, period
    plugin = kwargs.pop("plugin", None)
    try:
        return render_template(template, args=kwargs, plugin=plugin, **get_template_params())
    except TemplateNotFound as e:
        errormsg = """
        Oops... '%s' does not exist! This could be because:<br>
          (a) we're still working on it<br>
          (b) you've found a bug<br>
          (c) you're attempting to poke holes - please report back if you find any! <br><br>
        """
        return errormsg % template, 404

@app.route("/api/<plugin>/<method>", methods=["GET", "POST"])
def api(plugin, method):
    """

    :param plugin:
    :param method:
    :return:
    """
    kwargs = request.args.to_dict()  # deployment_id, platform_id, period
    template = kwargs.pop("template", False)
    if request.method == "POST":
        kwargs.update(request.json)
    try:
        resp = run_plugin_method(plugin, method, kwargs=kwargs)
        if template:
            return render_template(template, args=kwargs, data=resp, plugin=plugin, method=method, **get_template_params())
        else:
            return resp if isinstance(resp, flask_response_type) else jsonify(resp)
    except APIException as e:
        traceback.print_exc()
        return jsonify(message="API Error {}.{} (code: {}): {}.".format(plugin, method, e.code, e)), e.code
    except ConnectionError as e:
        traceback.print_exc()
        return jsonify(message="Unable to connect {}.{} (code: {}).".format(plugin, method, 503)), 503
    except Exception as e:
        traceback.print_exc()
        return jsonify(message="Error in {}.{}: {}.".format(plugin, method, e)), 500

@app.route("/settings/<action>", methods=["GET", "POST"])
@app.route("/settings", methods=["GET", "POST"], defaults={"action": None})
def settings(action):
    """

    :return:
    """
    if action == "reload":
        try:
            load_settings()
        except Exception as e:
            traceback.print_exc()
            return jsonify(message=str(e)), 500
    return topjson(dict(default=default_config_params, current=app.config))
    # return jsonify(default=default_config_params, current=app.config)

#######################################################################################################################
# Handy methods
#######################################################################################################################
def get_template_params():
    """
    Return dict with some useful properties to return to templates
    :return:
    """
    return dict(platform_plugin=current_platform_plugin, user=logged_in_user, ts=int(time.time()))

def run_plugin_method(plugin, method, args=[], kwargs={}):
    """

    :param plugin:
    :param method:
    :param args:
    :param kwargs:
    :return:
    """
    cls = application_plugins.get(plugin)
    if not hasattr(cls, method):
        print(cls)
        raise APIException("The '{}' plugin has no method named '{}'".format(plugin, method), code=404)
    fnc = getattr(cls, method)
    return fnc(*args, **kwargs)

def load_module_class(ref, args=[], kwargs={}):
    """

    :param ref:
    :param args:
    :param kwargs:
    :return:
    """
    class_module, class_name = ref.rsplit(".", 1)
    obj = getattr(import_module(class_module), class_name)
    return obj(*args, **kwargs)

def load_plugins(plugins):
    """

    :param plugins:
    :return:
    """
    for p,v in plugins.items():
        application_plugins[p] = load_module_class(v["ref"], args=v.get("args", []), kwargs=v.get("kwargs", {}))

def load_platform_plugins(platform_plugins):
    """

    :param platform_plugins:
    :return:
    """
    for p,v in platform_plugins.items():
        application_plugins[p] = load_module_class(v["plugin"], kwargs=dict(
            api_key=v.get("api_key"), host_url=v.get("host_url"), instance_id=p, settings=app.config))

def load_settings():
    try:
        app.config.from_json(app.config.get("USER_CONFIG"))  # update config with local user config from settings
        load_plugins(app.config.get("PLUGINS"))
        load_platform_plugins(app.config.get("PLATFORM_PLUGINS"))
    except Exception as e:
        raise APIException(f"ERROR loading config '{app.config.get('USER_CONFIG')}' ({e})")


def is_alive():
    """

    :return:
    """
    try:
        r = requests.head("http://localhost:{}".format(app.config.get("PORT")))
        return r.ok
    except Exception as e:
        return False

def start_server(debug=False):
    """

    :return:
    """
    # Load plugins
    try:
        load_settings()
    except Exception as e:
        traceback.print_exc()
        print(f"\n***\n{e}\n***\n")

    

    # Start webserver
    if debug:
        app.debug = True  # if running just server, run in debug mode

    # use gevent WSGI server instead of the Flask
    #http = WSGIServer(('', app.config.get("PORT")), app.wsgi_app)
    # TODO gracefully handle shutdown
    #http.serve_forever()
    
    app.run(host=app.config.get("HOST"), port=app.config.get("PORT"), threaded=True)
    # else:
    #     http_server = WSGIServer(("", app.config.get("PORT")), app)
    #     http_server.serve_forever()


def topjson(data):
    def default(o):
        if isinstance(o, (datetime.date, datetime.datetime)):
            return o.isoformat()

    return json.dumps(
        data,
        sort_keys=True,
        indent=2,
        default=default
    )


if __name__ == "__main__":
    # args = parser.parse_args()
    start_server(debug=True)
