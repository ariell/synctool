#!/usr/bin/env bash
CLOUD_DLD_DIR="/data/tpac_nfs/synctool/downloads/"
rsync -Pav -e "ssh -i ~/.ssh/cloud.key" dist_versions/* ubuntu@144.6.225.14:${CLOUD_DLD_DIR}