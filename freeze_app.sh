#!/usr/bin/env bash

OS="macos"
VERSION=`cat version`
INSTALL_DIR="dist_versions/${OS}/${VERSION}"

#pyinstaller -p . --onefile -w freeze_app.spec plugins/*.py
pyinstaller --onefile -w freeze_app.spec plugins/*.py

# Create distributable installer
echo -e "\nCreate distributable installer"
mkdir dist/dist_osx
rm -r dist/dist_osx/SyncTOOL.app
mv dist/SyncTOOL.app dist/dist_osx/
ln -s /Applications dist/dist_osx/
hdiutil create dist/tmp.dmg -ov -volname "SyncToolInstall" -fs HFS+ -srcfolder "dist/dist_osx/"

# Move to dist_versions directory
echo -e "\nMove to dist_versions directory"
mkdir -p ${INSTALL_DIR}
rm ${INSTALL_DIR}/SyncToolInstall.dmg
hdiutil convert dist/tmp.dmg -format UDZO -o ${INSTALL_DIR}/SyncToolInstall.dmg
rm dist/tmp.dmg

# Upload to cloud
echo -e "\nUpload to cloud"
read -p "Do you want to sync your build to the cloud? <y/N> " prompt
if [[ $prompt =~ [yY](es)* ]]; then
    bash sync_dist_versions.sh
fi
